<?php
namespace avanto;

class OnOfficeUnlockProviderIFrame
{
    /** */
    const GET_PARAMETER_TOKEN = 'token';
    /** */
    const GET_PARAMETER_CACHE_ID = 'parameterCacheId';
    /** */
    const GET_PARAMETER_SIGNATURE = 'signature';

    //secret created by command : echo -n "ESYSYNC_SIGNATURE" | openssl sha256 -hmac "HIONOFFICE"
    const SHARED_SECRET='e73f725c5d954c518df71aaada2d4fc250ad25d3274e1e954443c16f2dc049a1';

    /** @var string */
    private $_getParameterToken;
    /** @var string */
    private $_getParameterCacheId;

    /**
     *
     */

    public function __construct()
    {
        $this->_getParameterToken = $_REQUEST[self::GET_PARAMETER_TOKEN];
        $this->_getParameterCacheId =$_REQUEST[self::GET_PARAMETER_CACHE_ID];
    }
    /**
     *
     */
    public function createHtmlCode()
    {
        if ($this->checkSignature())
        {
            $this->printHtml();
        }
        else

        {
            $this->printErrorMessage();
        }
    }

    /**
     *
     * Just a demo function for checking the signature - contains pseudo code
     * @return bool
     *
     */
    private function checkSignature()
    {
        $signature = $this->getRequestVariable(self::GET_PARAMETER_SIGNATURE);
        $uriToCheck = $this->getUriToCheck();
        $checkSignature = hash_hmac('sha256', $uriToCheck, self::SHARED_SECRET);
        return (0 === strcmp($checkSignature, $signature));
    }
    /**
     *
     */
    private function printErrorMessage()
    {
        $html = '
<html>
    <head></head>
    <body' . $this->attr('style', 'margin: 0;') . '>
        <label>Signatur nicht korrekt</label>
    </body>
</html>';
        echo $html;
    }
    /**
     *
     */
    private function printHtml()
    {
            $cssUri = $this->getUriPath('/gui/smart/resources/css/layout/smart/themes/marketplace/marketplaceUnlockIFrame.css');
        $jsUri = 'js/unlockProvider.js';
        $unlockProviderData = $this->createUnlockProviderData();
        $unlockLabelText = $this->getUnlockLabelText();
        $html = '
<html>
    <head>
        <link' . $this->attr('rel', 'stylesheet') . $this->attr('type', 'text/css') . $this->attr('href', $cssUri) . '>
    </head>
    <body' . $this->attr('style', 'margin: 0;') . '>
        <label' . $this->attr('class', 'data-privacy-statement-label') . '>Datenschutzerklärung</label>
        <input' . $this->attr('class', 'data-privacy-statement') . $this->attr('id', 'dataPrivacyStatement') . $this->attr('type', 'checkbox') . $this->attr('onchange', 'checkboxChanged(this)') . '>
        <label' . $this->attr('class', 'data-privacy-link-prefix-label') . '>Ich akzeptiere die
            <a' . $this->attr('href', 'https://de.onoffice.com/') . $this->attr('target', '_blank') . '>
                    Datenschutzerklärung von *Hier Anbieter einsetzen*
            </a>
        </label>
        <label' . $this->attr('class', 'api-key-label') . '>API-Key</label>
       <div' . $this->attr('class', 'api-key-input-outer') . $this->attr('id', 'secretDiv') . '>
           <input' . $this->attr('class', 'api-key-input') . $this->attr('name', 'secret') . $this->attr('type', 'text') . $this->attr('placeholder', 'API-key hier reinkopieren') . $this->attr('id', 'secret') . '></div><input' . $this->attr('class', 'unlock-now-button') . $this->attr('id', 'unlockButton') . $this->attr('type', 'button') . $this->attr('value', $unlockLabelText) . $this->attr('title', 'Bitte zuerst der Datenschutzerklärung zustimmen') . $this->attr('alt', 'Bitte zuerst der Datenschutzerklärung zustimmen') . $this->attr('onclick', 'unlockProvider(' . $this->json($unlockProviderData) . ')') . '
            disabled>
        <label' . $this->attr('id', 'successMessage') . $this->attr('class', 'active-label') . '>Aktiv</label>
        <div' . $this->attr('id', 'spinner') . $this->attr('class', 'unlock-load-spinner') . '></div>
    </body>
    <script ' . $this->attr('src', $jsUri) . '></script>
</html>';
        echo $html;
    }
    /**
     *
     * @return array
     *
     */
    private function createUnlockProviderData()
    {
        $unlockProviderData = ['getParameterToken' => $this->_getParameterToken, 'getParameterCacheId' => $this->_getParameterCacheId, ];
        return $unlockProviderData;
    }

    /**
     *
     * @param string $name
     * @param string $value
     * @return string
     *
     */
    private function attr($name, $value)
    {
        $attr = ($value !== null ? ' ' . $name . '="' . htmlspecialchars($value, ENT_QUOTES) . '"' : '');
        return $attr;
    }
    /**
     *
     * @param string $path
     * @return string
     *
     */
    private function getUriPath($path)
    {
        return 'https://smart.onoffice.de/smart' . $path;
    }
    /**
     *
     * @param mixed $variable
     * @return string
     *
     */
    private function json($variable)
    {
        return json_encode($variable, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS);
    }

    /**
     *
     * @return string
     *
     */

    private function getUnlockLabelText()
    {
        return 'Jetzt Freischalten';
    }

    function getRequestVariable($name)
    {
        $value = '';

        if (array_key_exists($name, $_REQUEST))
        {
            $value = $_REQUEST[$name]??null;
        }
        return $value;
    }

    private function hash_equals($str1, $str2) {
        $str1_len = strlen($str1);
        $str2_len = strlen($str2);

        // Calculate XOR
        $diff = $str1_len ^ $str2_len;
        for($x = 0; $x < $str1_len && $x < $str2_len; $x++) {
            $diff |= ord($str1[$x]) ^ ord($str2[$x]);
        }

        return $diff === 0;
    }

    private function verifySignature($string_to_verify, $signature, $shared_secret) {
        return $this->hash_equals(hash_hmac("sha512", $string_to_verify, $shared_secret), $signature);
    }

    private function getUriToCheck()
    {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $pattern='/&'.self::GET_PARAMETER_SIGNATURE.'.*/';
        $actual_link=preg_replace($pattern, "", $actual_link);
        return $actual_link;
    }


}