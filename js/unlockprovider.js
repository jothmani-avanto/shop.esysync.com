function unlockProvider(arguments) {
    var spinner = document.getElementById('spinner'),
        button = document.getElementById('unlockButton'),
        successMessage = document.getElementById('successMessage'),
        secretTextInput = document.getElementById('secret'),
        secretDiv = document.getElementById('secretDiv'),
        secret = secretTextInput.value;
    var token = arguments.getParameterToken,
    parameterCacheId = arguments.getParameterCacheId;
    spinner.style.visibility = 'visible';
    button.style.visibility = 'hidden';

    var data = new FormData();
    data.append('token', token);
    data.append('secret', secret);
    data.append('parameterCacheId', parameterCacheId);
    var httpRequest = new XMLHttpRequest();

    //Here a PHP script is called, which executes the 
    //unlockProvider API call (see below: Beispiel zum 
    //Freischalten der API). The javascript waits for the
    //answer of the API call and returns the answer to
    //the unlock popup. 
    var url = 'https://api.onoffice.de/api/stable/api.php';
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            spinner.style.visibility = 'hidden';
            var responseText = httpRequest.responseText.replace(/\'/gi, '');
            if (responseText === "active") {
                secretTextInput.style.visibility = 'hidden';
                secretDiv.style.visibility = 'hidden';
                successMessage.style.visibility = 'visible';
            } else {
                button.style.visibility = 'visible';
            }
            updateParent(responseText);
        }
        httpRequest.open('POST', url, true);
        httpRequest.send(data);
    };
}
    /**
     *
     * Here the result should simply be sent back to the activation
     * window. In case of success this should be the string 'active',
     * in case of error the error message which should be displayed
     * in the activation window
     *
     */
    function updateParent(message) {
        var target = window.parent;
        target.postMessage(message)
    };

    function checkboxChanged(checkbox) {
        var button = document.getElementById('unlockButton');
        if (checkbox.checked) {
            button.removeAttribute('disabled');
            button.removeAttribute('title');
            button.removeAttribute('alt');
        } else {
            var text = 'Bitte zuerst der Datenschutzerklärung zustimmen';
            button.setAttribute('disabled', 'disabled');
            button.setAttribute('title', text);
            button.setAttribute('alt', text);
        }
    }
